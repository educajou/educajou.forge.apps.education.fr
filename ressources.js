/*

Pour ajouter une nouvelle ressource :
- ajouter une image au format png dans le dossier images, portant le nom du projet ou du groupe s'il n'y a pas de nom de projet renseigné
- ajouter ci-dessous une nouvelle ressource sur le modèle des autres

*/

// ATTTENTION : le lien du projet c'est celui de la page côté Forge. Elle commence par https://forge.apps.education.fr/GROUPE...
// et non par https://GROUPEouPROJET.forge.apps.education.fr/


// Types de ressources

const webapp = '🌐 Application web';
const logiciel = '📥 Logiciel';
const site = '📖 Site documentaire';
const documents = '📄 Documents';
const os = "💿 Système d'exploitation";


async function creerTableauRessource() {

    ajouterRessource(
        webapp,
        "SkyJou",
        "Compter ou comparer des points en utilisant des procédures de calcul : doubles, compléments à 10 ...",
        ["mathématiques",'calcul','procédure','procédural','addition','soustraction','jeu'],
        "https://forge.apps.education.fr/educajou/skyjou",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "MultiCalc",
        "Travailler de façon ludique le calcul réfléchi en utilisant les propriétés du calcul et de la numération",
        ["mathématiques",'calcul','procédure','procédural','addition','soustraction','multiplication','multiples','jeu'],
        "https://forge.apps.education.fr/educajou/multicalc",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Orbito",
        "Modéliser de façon simplifiée le système Terre Lune Soleil",
        ["sciences",'astronomie','ciel','espace','orbite','représenter','schéma','physique'],
        "https://forge.apps.education.fr/educajou/orbito",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Dictia",
        "Créer une dictée personnalisée d'après des paramètres, en utilisant de l'intelligence artificielle",
        ["orthographe",'ia','générateur','générer','création','ia','prompt','français'],
        "https://forge.apps.education.fr/educajou/dictia",
        null,
        null
    );
    
    ajouterRessource(
        webapp,
        "Quel collège ?",
        "Afficher le collège de secteur d'après une adresse de résidence",
        ["cycle 3", "direction", "directrice", "directeur", "outil", "affectation", "organisation"],
        "https://forge.apps.education.fr/educajou/quelcollege",
        null,
        null
    );

    ajouterRessource(
        logiciel,
        "Fracatux",
        "Représenter les fractions sous formes de barres",
        ["mathématiques",'numération','fractions',"Primtux"],
        "https://forge.apps.education.fr/educajou/fracatux",
        null,
        null
    )

    ajouterRessource(
        webapp,
        "Seyes",
        "Afficher une page de cahier et écrire en cursive",
        ["tableau", "écriture", "cahier", "français", "Primtux"],
        "https://forge.apps.education.fr/educajou/seyes",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "ÉcoleMap",
        "Afficher une vue aérienne, le plan du quartier, les informations d'une école à partir de son UAI (RNE)",
        ["ppms", "gardes", "plan", "carte", "cartographie", "geoportail", "openstreetmap", "data", "annuaire", "éducation", "organisation"],
        "https://forge.apps.education.fr/educajou/ecolemap",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Tuxprompt",
        "Faire défiler un texte à la la façon d'un prompteur",
        ["lecture", "français", "fluence", "Primtux"],
        "https://forge.apps.education.fr/educajou/tuxprompt",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Flashmots",
        "Exercer sa mémoire orthographique",
        ["orthographe", "français", "Primtux"],
        "https://forge.apps.education.fr/educajou/flashmots",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Chocomultiples",
        "Représenter les multiplications sous forme de tablettes de chocolat",
        ["mathématiques", "calcul", "tables", "chocolat", "Primtux"],
        "https://forge.apps.education.fr/educajou/chocomultiples",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Répartition",
        "Organiser la répartition des classes dans une école primaire",
        ["école",'direction',"organisation","Primtux"],
        "https://forge.apps.education.fr/educajou/repartition",
        null,
        null
    )

    ajouterRessource(
        webapp,
        "Rapidétik",
        "Créer rapidement des exercices avec étiquettes mobiles à partir d'une phrase",
        ["français", "lecture", "production d'écrit", "Primtux"],
        "https://forge.apps.education.fr/educajou/rapidetik",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Remix",
        "Ventiler des groupes hétérogènes d'élèves",
        ["école", "direction", "organisation"],
        "https://forge.apps.education.fr/educajou/remix",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Estimation",
        "S'exercer au repérage sur la droite non graduée",
        ["mathématiques", "numération", "entiers", "Primtux"],
        "https://forge.apps.education.fr/educajou/estimation",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Lis pour moi",
        "Transformer une phrase en CodeQR avec synthèse vocale",
        ["lecture", "outil", "autonomie", "adaptation", "français"],
        "https://forge.apps.education.fr/educajou/lispourmoi",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Écris pour moi",
        "Dictionnaire vocal (Android & iPad) - obtenir l'orthographe d'un mot prononcé",
        ["écriture", "outil", "autonomie", "adaptation", "français","production d'écrit"],
        "https://forge.apps.education.fr/educajou/ecrispourmoi",
        null,
        "ecrispourmoi.jpg"
    );

    ajouterRessource(
        webapp,
        "Droite interactive",
        "Afficher une droite graduée (nombre entiers) paramétrable",
        ["mathématiques", "numération", "calcul", "entiers", "Primtux"],
        "https://forge.apps.education.fr/educajou/droite-interactive",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Compteur",
        "Afficher et faire fonctionner un compteur décimal",
        ["mathématiques", "numération", "entiers", "décimaux", "nombres", "chiffres", "Primtux"],
        "https://forge.apps.education.fr/educajou/compteur",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "AutoBD",
        "Créer facilement une bande dessinée et l'exporter en image",
        ["français", "arts", "BD", "multimédia", "Primtux"],
        "https://forge.apps.education.fr/educajou/autobd",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Automultiples",
        "Afficher rapidement les multiples d'un nombre",
        ["mathématiques", "calcul", "Primtux"],
        "https://forge.apps.education.fr/educajou/automultiples",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Tuxtimer",
        "Afficher un timer de 0 à 60 minutes",
        ["classe", "organisation", "temps", "maternelle", "Primtux"],
        "https://forge.apps.education.fr/educajou/tuxtimer",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Tétri Math",
        "Jeu d'arcade façon tétris sur les compléments à 10, 20, 100",
        ["mathématiques", "calcul", "jeu"],
        "https://forge.apps.education.fr/educajou/tetrimath",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Minimages",
        "Réduire un lot d'images",
        ["école", "classe", "outil", "rgpd", "direction", "multimédia", "organisation", "Primtux"],
        "https://forge.apps.education.fr/educajou/minimages",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Syllabux",
        "Un syllabaire interactif. Composer ses syllabes, choisir les graphèmes, faire défiler...",
        ["français", "lecture", "cycle 2", "phonologie", "Primtux"],
        "https://forge.apps.education.fr/educajou/syllabux",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "ELF",
        "Afficher un diaporama automatique de lecture flash à partir d'une liste de mots",
        ["français", "lecture", "cycle 2", "fluence", "Primtux"],
        "https://forge.apps.education.fr/educajou/elf",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Motminot",
        "Un mot par jour à deviner façon Motus. Un thème par semaine",
        ["français", "lecture", "cycle 2", "vocabulaire", "Primtux"],
        "https://forge.apps.education.fr/educajou/motminot",
        null,
        null
    );

    ajouterRessource(
        webapp,
        "Graduatux",
        "S'exercer au repérage sur la droite graduée",
        ["mathématiques", "numération", "entiers", "décimaux", "Primtux"],
        "https://forge.apps.education.fr/educajou/graduatux",
        null,
        null
    );

    ajouterRessource(
        logiciel,
        "Tuxblocs",
        "Représenter les unités, dizaines, centaines et milliers",
        ["mathématiques",'numération',"Primtux"],
        "https://forge.apps.education.fr/educajou/tuxblocs",
        null,
        null
    )
}





