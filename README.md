# Portail premier degré de la Forge des Communs numériques Éducatifs

Cette page https://primaire.forge.apps.education.fr/ a pour but de proposer des ressources de la Forge utiles aux enseignants du premier degré.

## Ajouter une ressource

Pour ajouter une ressource suivez ces étapes :
- Créez et ajoutez une vignette
- Modifiez le fichier `ressources.js`

### Ajouter une vignette
- Téléchargez le logo (s’il est assez grand) ou une effectuez une copie d’écran de l’application,
- Donnez les dimensions `400px x 400px` à l’image, format png,
- Nommez l'image d'après l'identifiant du projet ou du groupe tel qu'il existe sur la Forge (par exemple pour `https://forge.apps.education.fr/nicolas-taffin-ext/ada-lelivre`, nommez la vignette **ada-lelivre.png** ou pour `https://forge.apps.education.fr/flipbook/flipbook.forge.apps.education.fr`nommez la vignette **flipbook.png**).
- Déposez-là dans le dossier **[images/projets/](https://forge.apps.education.fr/primaire/primaire.forge.apps.education.fr/-/tree/main/images/projets?ref_type=heads)**,

### Éditer le fichier ressources.js

On peut [modifier directement le fichier ressources.js en ligne](https://forge.apps.education.fr/primaire/primaire.forge.apps.education.fr/-/edit/main/ressources.js?ref_type=heads) (méthode la plus simple) ou bien le télécharger localement puis le synchroniser (méthode nécessitant de paramétrer Git).
Dans les deux cas, après avoir terminé les modifications, ne pas oublier d'enregistrer avec un message de commit explicite, dans la branche "main".
![](images/commit.png)

Pour ajouter une ressource, il suffit de copier-coller une des ressources déjà présentes, de la modifier et de l'ajouter à la suite des autres dans le fichier `ressources.js`.
Il faut impérativement **conserver la structure** :
- Les paramètres sont séparés par des virgules (voir en fin de ligne).
- Les URL et chaînes textuelles doivent être entre guillemets ou entre apostrophes.
- la liste des mots-clés doit être entre crochets, et chaque mot-clé séparé par une virgule.
- On peut supprimer, si on veut, les lignes commençant par // qui ne sont que des commentaires explicatifs.

**Il y a 7 paramètres à spécifier.**


#### 1. Type de ressource

Écire une de ces clés **sans guillemets** :

| Clé        | Ce qui s'affichera          |
|------------|-----------------------------|
| webapp     | 🌐 Application web         |
| logiciel   | 📥 Logiciel                |
| site       | 🗂️ Site documentaire       |
| documents  | 📄 Documents               |
| os         | 💿 Système d'exploitation  |

#### 2. Titre

Entre guillemets ou apostrophes, rester court.

#### 3. Description

Entre guillemets ou apostrophes, rester court.

#### 4. Liste des mots-clés

On peut ajouter autant de mots-clés que nécessaire, en conservant bien la structure :
`["mot-clé","mot-clé","mot-clé", ...],`

#### 5. Lien du projet sur la Forge

Attention, il s'agit du lien de page côté Gitlab, pas de la page publique !

![](images/urlprojet.png)

#### 6. Page publique

Si la page publique est sur la Forge, écrire **null** sans guillemets.
Si la page publique n'est pas sur la forge, mettre le lien entre guillemets.

#### 7. Vignette

Si l'image est nommée d'après le nom du projet ou du groupe, laisser **null**.
Sinon, donner le nom de l'image entre guillemets (exemple : **"fractor2000.png"**).
Cela permet de gérer les cas où plusieurs ressources utilisent le même nom de projet ou de groupe.

### Exemples

#### Ressource dont la page publique est sur la Forge

```` javascript

    ajouterRessource(
        webapp,
        "CPC 2049",
        "Un outil pour créer un compte-rendu de visite automatisé",
        ["outil", "conseiller pédagogique",'formation'],
        "https://forge.apps.education.fr/professeurherve/cpc2049",
        null,
        null
    )

````

#### Ressource dont la page publique n'est pas sur la Forge

```` javascript

    ajouterRessource(
        webapp,
        "MathALÉA",
        "Générer des exercices de mathématiques interactifs ou en PDF",
        ['outil','mathématiques','cycle 3'],
        "https://forge.apps.education.fr/coopmaths/mathalea",
        "https://coopmaths.fr/alea/",
        null
    )

````

#### Ressource dont la page publique n'est pas sur la Forge avec nom de l'image personnalisé

```` javascript

    ajouterRessource(
        webapp,
        "Rituel de problèmes",
        "Chaque jour d'école, pour chaque niveau, un nouveau problème selon une progression",
        ['problèmes','mathématiques','cycle 2','cycle 3','maths en vie'],
        "https://forge.apps.education.fr/remi.gilger-ext/les-apps-maths-en-vie",
        "https://appenvie.fr/rituel",
        "rituel.png"
    )

````



## Licence

Le site est sous licence Creative Commons BY SA 4.0.

La police de caractères utilisée pour le titre est "Romain" de [Éduscol](https://eduscol.education.fr/204/polices-de-caracteres-cursives-pour-l-enseignement-de-l-ecriture), sous licence Creative Commons BY ND.
