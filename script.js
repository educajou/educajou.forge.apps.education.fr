// Constituants de la page

const divContenus = document.getElementById('contenus');
const divInfos = document.getElementById('texte-chapeau');
const boutonInfos = document.getElementById('bouton-infos');
const divPanneau = document.getElementById('panneau');
const boutonProposerBas = document.getElementById('proposer-bas');
const divInfosPrimtux = document.getElementById('divInfosPrimtux');
const boutonToutesCategories = document.getElementById('boutonToutesCategories');

let categories = ['Mathématiques','Français','Organisation']; 
let categoriesActives = [];



document.addEventListener("DOMContentLoaded", async () => {
    try {
        verifierUrl();
        await creerTableauRessource();
        ressources = rangerTableauAlpha(ressources);
        await afficheRessources();        
    } catch (error) {
        console.error("Erreur lors de l'initialisation :", error);
    }
});

function verifierUrl(){
    // Récupérer les paramètres de la requête dans l'URL
    const urlParams = new URLSearchParams(window.location.search);

    // Vérifier si un paramètre "categorie" est présent dans l'URL
    const rechercheCategorie = urlParams.get('categorie');

    if (rechercheCategorie !='' && rechercheCategorie!=null){
        console.log("Catégorie dans l'URL:",rechercheCategorie);
        let bouton = getButton(rechercheCategorie);
        filtreCategories(bouton);
    } else {
        filtreCategories(boutonToutesCategories);
    }
}

async function afficheRessources() {
    try {
        console.log(`Création de ${ressources.length} ressources`);

        for (const ressource of ressources) {
            // Simulation d'une opération asynchrone (ex: fetch d'images ou autres métadonnées)
            await new Promise(resolve => setTimeout(resolve, 0)); // Supprimez cette ligne si inutile
            
            let nouvelleCase = document.createElement('div');
            nouvelleCase.classList.add('case-ressource');

            let nouveauType = document.createElement('div');
            nouveauType.innerHTML = ressource.type;
            nouveauType.classList.add('type-ressource');

            let nouveauTitre = document.createElement('h1');
            nouveauTitre.innerHTML = ressource.titre;
            nouveauTitre.classList.add('titre-ressource');

            let nouvelleImage = document.createElement('div');
            nouvelleImage.style.backgroundImage = `url(${ressource.image})`;
            nouvelleImage.classList.add('image-ressource');

            let nouveauLien = document.createElement('a');
            nouveauLien.href = ressource.urlRessource;

            let nouvelleDescription = document.createElement('p');
            nouvelleDescription.innerHTML = ressource.description;
            nouvelleDescription.classList.add('description-ressource');

            let nouvelleDivBoutons = document.createElement('div');
            nouvelleDivBoutons.classList.add('boutons-ressource');

            let nouveauBoutonUtiliser = document.createElement('a');
            nouveauBoutonUtiliser.innerHTML = "Accéder";
            nouveauBoutonUtiliser.href = ressource.urlRessource;
            nouveauBoutonUtiliser.target = '_blank';
            nouveauBoutonUtiliser.title = "Accéder à cette ressource";
            nouveauBoutonUtiliser.classList.add('bouton-ressource', 'utiliser');

            let nouveauBoutonExplorer = document.createElement('a');
            nouveauBoutonExplorer.innerHTML = "Voir le code";
            nouveauBoutonExplorer.href = ressource.urlProjet;
            nouveauBoutonExplorer.target = '_blank';
            nouveauBoutonExplorer.title = "Voir à l'intérieur (code source du projet)";
            nouveauBoutonExplorer.classList.add('bouton-ressource', 'explorer');

            let nouveauBoutonTicket = document.createElement('a');
            nouveauBoutonTicket.innerHTML = "Tickets";
            nouveauBoutonTicket.href = ressource.urlTickets;
            nouveauBoutonTicket.target = '_blank';
            nouveauBoutonTicket.title = "Rédiger un nouveau ticket pour rapporter un bug ou suggérer une amélioration";
            nouveauBoutonTicket.classList.add('bouton-ressource', 'ticket');

            nouvelleCase.appendChild(nouveauType);
            nouvelleCase.appendChild(nouveauTitre);
            nouveauLien.appendChild(nouvelleImage);
            nouvelleCase.appendChild(nouveauLien);
            nouvelleCase.appendChild(nouvelleDescription);

            nouvelleDivBoutons.appendChild(nouveauBoutonExplorer);
            nouvelleDivBoutons.appendChild(nouveauBoutonUtiliser);
            nouvelleDivBoutons.appendChild(nouveauBoutonTicket);

            nouvelleCase.appendChild(nouvelleDivBoutons);

            let motsCles = ressource.tags.map(tag => normaliser(tag));
            traiteVisibilite(nouvelleCase,motsCles);

            divContenus.appendChild(nouvelleCase);
        }
    } catch (error) {
        console.error("Erreur lors de l'affichage des ressources :", error);
    }
}



function melangerTableau(tableau) {
    for (let i = tableau.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [tableau[i], tableau[j]] = [tableau[j], tableau[i]]; // Échange des éléments
    }
    return tableau;
}

function rangerTableauAlpha(tableau) {
    return tableau.sort((a, b) => {
        return a.titre.localeCompare(b.titre, 'fr', { sensitivity: 'base' });
    });
}




function derouleInfos() {
    divInfos.classList.toggle('visible');
    setTimeout(() => {
        boutonInfos.classList.toggle('visible');
    }, 500);    
}




function getButton(categorie) {
    console.log('getbutton',categorie)
    const boutons = document.querySelectorAll('#categories > button');
    let bouton;
    boutons.forEach(boutonAExaminer => {
        if (normaliser(boutonAExaminer.value)===normaliser(categorie)) {
            bouton = boutonAExaminer;
            console.log('bouton trouvé',boutonAExaminer.value);
        }
    });
    return bouton;
}

function filtreCategories(bouton) {

    console.log("Filtre catégorie",bouton.value);

    const boutons = document.querySelectorAll('#categories > button');
    boutons.forEach(boutonAExaminer => {
        if (boutonAExaminer != bouton) {
            boutonAExaminer.classList.remove('actif');
        } else {
            bouton.classList.add('actif');
        }
    });

    let categorie = normaliser(bouton.value);

    if (categorie==="primtux" && bouton.classList.contains('actif')) {
        divInfosPrimtux.classList.remove('hide');
    } else {
        divInfosPrimtux.classList.add('hide');
    }

    categoriesActives = [];    
    
    // Vérifie si la catégorie est déjà active
    if (categoriesActives.includes(categorie)) {
        // Si oui, la supprimer
        categoriesActives = categoriesActives.filter(cat => cat !== categorie);
    } else {
        // Sinon, l'ajouter
        categoriesActives.push(categorie);
    }
    
    if (categorie==='toutes') {
        console.log('toutes catégories');
        categoriesActives = [];
    }

    // Normaliser les catégories actives
    categoriesActives = categoriesActives.map(normaliser);
    
    console.log('Catégories actives',categoriesActives.join(','))

    // Récupérer toutes les tuiles
    const tuiles = document.querySelectorAll('.case-ressource');

    // Masquer toutes les tuiles
    tuiles.forEach(tuile=>{tuile.classList.add('categorieNonActive')});

    // Passer en revue chaque tuile
    let i = 0;

    if (categoriesActives.length === 0) {
        tuiles.forEach(tuile => {
            tuile.classList.remove('categorieNonActive');
        });
    } else {
        tuiles.forEach(tuile => {
            let motsCles = ressources[i].recupererMotsCles(); // Récupérer les tags comme tableau
            traiteVisibilite(tuile,motsCles);       
            i++;
        });
        
    }

    const url = new URL(window.location); // Crée un objet URL à partir de l'URL actuelle
    url.searchParams.set('categorie', categorie); // Met à jour ou ajoute le paramètre 'categorie'
    history.pushState(null, '', url); // Met à jour l'URL sans recharger la page
}

function traiteVisibilite(tuile,motsCles) {
    console.log('Traitement visibilité',tuile,motsCles.join(','));
    // Vérifie si tous les mots-clés actifs sont dans les mots-clés de la ressource
    if (categoriesActives.every(motCle => motsCles.includes(motCle))) {
        tuile.classList.remove('categorieNonActive'); // Afficher la tuile
    } else {
        tuile.classList.add('categorieNonActive'); // Masquer la tuile
    }
}

function majURL(recherche) {
    // Nettoyer la chaîne : supprimer les espaces multiples et les remplacer par un espace unique
    let rechercheNettoyee = recherche.replace(/\s+/g, ' ').trim();

    // Encoder les caractères spéciaux et remplacer les espaces par des "+"
    let rechercheEncodee = encodeURIComponent(rechercheNettoyee).replace(/%20/g, '+');

    // Récupérer l'URL actuelle et les paramètres existants
    const url = new URL(window.location.href);

    // Mettre à jour ou ajouter le paramètre "recherche"
    url.searchParams.set('recherche', rechercheEncodee);

    // Mettre à jour l'URL dans la barre d'adresse sans recharger la page
    window.history.replaceState({}, '', url);
}


function normaliser(texte) { // Passer un texte en minuscules sans accents
    
    const texteNormalise = texte
    .normalize("NFD") // Sépare les lettres des accents
    .replace(/[\u0300-\u036f]/g, "") // Supprime les accents
    .toLowerCase(); // Convertit en minuscules

    return texteNormalise;

}

function proposer() {
    window.scrollTo({
        top: document.body.scrollHeight,
        behavior: 'smooth' // Défilement fluide
      });
    boutonProposerBas.classList.add('warning');
}