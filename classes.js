let ressources = [];

class Ressource {
    constructor(type,titre, description, tags = [], urlProjet, urlRessource,image) {

        // Récupération des données fournies dans ressources.js
        this.type = type;
        this.titre = titre;
        this.description = description;
        this.tags = tags;
        this.urlProjet = urlProjet;

        // Contsruction des informations
        let infos = this.recupererInfos(urlProjet);
        let groupe = infos[0];
        let projet = infos[1];

        // En priorité urlRessource fournie, sinon on la devine d'après le fonctionnement de la Forge
        if (urlRessource) {this.urlRessource = urlRessource;}
        else {this.urlRessource = infos[2];}

        // En priorité l'image indiquée ...
        if (image){this.image = `images/projets/${image}`;}
        // ... sinon projet.png ...
        else if (projet){this.image = `images/projets/${projet}.png`;}
        // ... sinon groupe.png
        else {this.image = `images/projets/${groupe}.png`;}

        // Construction de l'URL de la ressource et du projet
        this.urlTickets = `${this.urlProjet}/-/issues`;

    }

    recupererInfos(url) {
    
        // Enlever les espaces ou les slashs de fin
        url = url.trim().replace(/\/+$/, '');
    
        // Enlever le préfixe 'https://forge.apps.education.fr/'
        const baseUrl = 'https://forge.apps.education.fr/';
        if (url.startsWith(baseUrl)) {
            url = url.replace(baseUrl, '');
        }
    
        // Couper la chaîne au premier "/"
        let groupe = url.split('/')[0];
        let projet = url.split('/')[1];
        if (projet.includes('.'))  {
            projet = null;
        }
        let urlRessource;
        
        if (projet) {
            urlRessource = `https://${groupe}.forge.apps.education.fr/${projet}`;
        } else {
            urlRessource = `https://${groupe}.forge.apps.education.fr`;
        }
        
        

        let infos = [groupe,projet,urlRessource];

        return infos;
    }
    
    
     

    // Méthode asynchrone pour récupérer les tags d'un projet et les ajouter
    async enrichirTagsDepuisAPI() {
        const proxyUrl = 'https://corsproxy.io/?';
        const url = `${proxyUrl}${this.urlProjet}`;
    
        try {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(`Erreur: ${response.statusText}`);
            }
            const html = await response.text();
            console.log(this.projet);
            console.log("HTML récupéré : ", html);
               
            // Utilisation d'une regex pour extraire les tags
            const regex = /<span class="gl-badge-content">(.*?)<\/span>/g; // Modifiez ce regex selon la structure HTML de votre page
            let tags = [];
            let match;
    
            while ((match = regex.exec(html)) !== null) {
                tags.push(match[1].trim());
            }
    
            return tags; // Retourne les tags trouvés
        } catch (error) {
            console.error("Erreur lors de la récupération des tags:", error);
            return [];
        }
    }
    

    // Méthode pour initialiser l'objet et récupérer les tags
    async init() {
        const tagsSuppelementaires = await this.enrichirTagsDepuisAPI();
        console.log("Tags récupérés : " + tagsSuppelementaires);
        this.tags.push(...tagsSuppelementaires);
    }

    // Méthode pour afficher les informations de l'objet dans la console
    afficherInfo() {
        console.log(`Titre: ${this.titre}`);
        console.log(`Description: ${this.description}`);
        console.log(`Groupe: ${this.groupe}`);
        console.log(`Projet: ${this.projet}`);
        console.log(`Tags: ${this.tags.join(', ')}`);
        console.log(`URL Ressource: ${this.urlRessource}`);
        console.log(`URL Projet: ${this.urlProjet}`);
    }

    recupererMotsCles() {
        return this.tags.map(tag => normaliser(tag)); // Retourne un tableau des tags normalisés
    }
    

    // Méthode pour récupérer le contenu des variables dans une chaîne unique
    recupererTextes() {
        return `${this.titre} ${this.description} ${this.groupe} ${this.projet} ${this.tags.join(', ')}`;
    }
}

async function ajouterRessource(type, titre, description, tags, urlProjet, urlRessource,image) {
    let ressource = new Ressource(type, titre, description, tags, urlProjet, urlRessource,image);
    ressources.push(ressource);
    //await ressource.init(); // Attendre que l'initialisation soit terminée
}

/// LISTE DES MOTS CLÉS DE PRIMTUX ///

const listeMotsClesPrimtux = [
    "120 s",
    "À la campagne",
    "Abulédu Associations",
    "Abuledu Microtexte",
    "Abuledu Minitexte",
    "Achats",
    "Activités Jclic",
    "Ada & Zangmann",
    "Additions fléchettes (Gcompris)",
    "Algorithmes (Gcompris)",
    "Alphabet (Gcompris)",
    "Anglais C3 (Jclic)",
    "Anki",
    "Apprends les additions (Gcompris)",
    "Apprends les chiffres (Gcompris)",
    "Apprends les nombres décimaux (Gcompris",
    "Association d'images",
    "Association d'images 2",
    "Associations géométriques (Gcompris)",
    "Au jardin",
    "Au tableau",
    "Audacity",
    "Auto BD",
    "Automultiples",
    "Avoir et être",
    "Balance virtuelle",
    "Baobab",
    "Blocs logiques",
    "Calcul-tables-logique C3  (Jclic)",
    "Calcul@TICE",
    "Calculatrice",
    "Campement (edit-interactive-svg)",
    "Catfish",
    "Celluloid",
    "Chiffres lettres",
    "Clavier souris (Jclic)",
    "Clique sur la lettre majuscule (Gcompris)",
    "Clique sur la lettre minuscule (Gcompris)",
    "Compte les éléments (Gcompris)",
    "Compte les intervalles (Gcompris)",
    "Conjugaison C3 (Aleccor)",
    "Conjugaison C3 (Jclic)",
    "Continents et océans C3 (Jclic)",
    "Convertir",
    "Créateur de clé USB",
    "Crée une collection (Gcompris)",
    "Croissant décroissant",
    "Décode le chemin (Gcompris)",
    "Décode le chemin relatif (Gcompris)",
    "Dessin",
    "Déterminant-nom-verbe-genre-nombre (Jclic)",
    "Devine l’opération (Gcompris)",
    "Devine le nombre (Gcompris)",
    "Digiscreen",
    "Dizaines",
    "Do.doc",
    "Edit interactive SVG",
    "Encode le chemin (Gcompris)",
    "Encode le chemin relatif (Gcompris)",
    "Encoder des mots",
    "Enrichis ton vocabulaire (Gcompris)",
    "Estimation",
    "États de la matière (edit-interactive-svg)",
    "Firefox",
    "Flashmots",
    "Fonctions C3 (Aleccor)",
    "Font Manager",
    "Fotowall",
    "Fracatux",
    "Français",
    "Gcompris",
    "Générateur d’emploi du temps",
    "GeoGebra",
    "Globe virtuel marble",
    "Goldendict",
    "gSpeech",
    "Histoires",
    "Homophones grammaticaux C3 (Aleccor)",
    "Homophones lexicaux C3 (Aleccor)",
    "Horloge École",
    "Hypnotix",
    "ImageMagick",
    "Imagination",
    "Jclic author",
    "Je lis puis j'écris des mots",
    "Je lis puis j'écris des phrases",
    "Je me présente",
    "Kazam",
    "Kdenlive",
    "Khangman",
    "Kiwix-Vikidia",
    "Kturtle",
    "L’écluse (Gcompris)",
    "L’ordre alphabétique",
    "La classe du lama",
    "La lettre dans le mot (Gcompris)",
    "La lettre manquante (Gcompris)",
    "La monnaie (Gcompris)",
    "La monnaie avec centimes (Gcompris)",
    "Labyrinthe (Gcompris)",
    "Labyrinthe de programmation (Gcompris)",
    "Labyrinthe invisible (Gcompris)",
    "Labyrinthe relatif (Gcompris)",
    "Le compte est bon (Nombre cible)",
    "Le conjugueur Linux",
    "LectOrBus",
    "Lecture C3 – Aleccor",
    "Lecture horizontale (Gcompris)",
    "Lecture verticale (Gcompris)",
    "Les contraires C3 (Aleccor)",
    "Les déchets C3 (Jclic)",
    "Les diviseurs (Gcompris)",
    "Les égalités (Gcompris)",
    "Les lettres qui tombent (Gcompris)",
    "Les mots qui tombent (Gcompris)",
    "Les multiples (Gcompris)",
    "Les nombres avec des dés (Gcompris)",
    "Les nombres avec des dominos (Gcompris)",
    "Les nombres romains (Gcompris)",
    "Les saisons (edit-interactive-svg)",
    "Lettre dans le mot (Gcompris)",
    "Leximots",
    "Lexique (Jclic)",
    "LibreOffice Base",
    "LibreOffice Calc",
    "LibreOffice Draw",
    "LibreOffice Impress",
    "LibreOffice Math",
    "LibreOffice Writer",
    "Logipix",
    "Logithèque",
    "Lumi",
    "Maternelle",
    "Mathématiques",
    "Memory additions contre Tux (Gcompris)",
    "Memory additions et soustractions contre Tux (Gcompris)",
    "Memory additions et soustractions seul (Gcompris)",
    "Memory additions seul (Gcompris)",
    "Memory divisions contre Tux (Gcompris)",
    "Memory divisions seul (Gcompris)",
    "Memory multiplications contre Tux (Gcompris)",
    "Memory multiplications divisions contre Tux (Gcompris)",
    "Memory multiplications divisions seul (Gcompris)",
    "Memory multiplications seul (Gcompris)",
    "Memory soustractions contre Tux (Gcompris)",
    "Memory soustractions seul (Gcompris)",
    "MenuLibre",
    "Mikado",
    "Minetest",
    "Monsieur Patate",
    "Motminot",
    "MultiMots",
    "Multiples",
    "Nature C3 (Aleccor)",
    "Nombres pairs et impairs (Gcompris)",
    "Nomme l’image (Gcompris)",
    "Numération CE1 (Jclic)",
    "Numération CP (Jclic)",
    "Numériseur de documents",
    "OCRFeeder",
    "Onboard on screen keyboard",
    "Openboard",
    "Ordonne les mots d’une phrase (Gcompris)",
    "Ordonne les nombres (Gcompris)",
    "Ordre alphabétique CE1-CE2 (Jclic)",
    "Orthodéfi",
    "Orthographe C3 (Aleccor)",
    "Osmo",
    "Organisation",
    "Outil de sauvegarde",
    "Partage les confiseries (Gcompris)",
    "Passé présent futur C2 (Jclic)",
    "PDFsam Basic",
    "Pendu peda GTK",
    "Pèse des objets avec une balance (Gcompris)",
    "Pinta",
    "Polygones C3 (Jclic)",
    "Positions (Gcompris)",
    "PoufPouf CP",
    "PoufPouf CE1",
    "PoufPouf CE2",
    "PoufPouf jeux",
    "Primtux",
    "PrimtuxMenu",
    "Puzz’n road",
    "Puzzles (Gcompris)",
    "Pylote",
    "qdictionnaire",
    "qStopMotion",
    "RapidÉtik",
    "Reconstruis le modèle (Gcompris)",
    "Rends la monnaie (Gcompris)",
    "Rends la monnaie avec centimes (Gcompris)",
    "Repérage",
    "Reproduis l’image (Gcompris)",
    "Reproduis l’image symétrique (Gcompris)",
    "Ri-li",
    "Rondissimo",
    "Sciences C3 (Aleccor)",
    "Scratch",
    "Scratch junior",
    "Screenshot",
    "Seyes",
    "Software-properties",
    "Sons complexes CE1 (Jclic)",
    "Sons CP (Jclic)",
    "SoundConverter",
    "Suffixes, préfixes C3 (Aleccor)",
    "Suite de nombres (Gcompris)",
    "Syllabux",
    "Synaptic",
    "Synonymes, familles C3 (Jclic)",
    "Système solaire (edit-interactive-svg)",
    "Tables d’addition (Gcompris)",
    "Tables de division (Gcompris)",
    "Tables de multiplication (Gcompris)",
    "Tables de soustraction (Gcompris)",
    "Tangram (Gcompris)",
    "Thunderbird",
    "Thunar",
    "Transmission",
    "Tri – rangement -intrus - images séquentielles (Jclic)",
    "Tuxblocs",
    "Tuxbot",
    "TuxPaint",
    "Tuxtimer",
    "Types de phrases C3 – Aleccor",
    "Valeurs des lettres g, j, s, z, k",
    "VLC média player",
    "WinFF",
    "Xed",
    "Xournal",
    "Zébulon"
]
