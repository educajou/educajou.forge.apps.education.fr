---
author: Éducajou
description: "Éducajou - Applications libres pour les élèves et les enseignants"
title: "Éducajou - Applications libres pour les élèves et les enseignants"
lang: fr
---

# Applications libres pour l'école

## Mathématiques

### Automultiples

![](img/automultiples.png) 

Afficher rapidement les multiples d'un nombre. Utile pour la technique opératoire de la division.

https://educajou.forge.apps.education.fr/automultiples/

### Tuxblocs

![](img/tuxblocs.png) 

Représenter, grouper, casser, compter ... les milliers, centaines, dizaines et unités sous formes de blocs de base


https://educajou.forge.apps.education.fr/tuxblocs/


### Fracatux

![](img/fracatux.png) 

Représenter, comparer, additionner ... des fractions sous forme de barres proportionnelles


https://educajou.forge.apps.education.fr/fracatux/


### Multiples

![](img/multiples.png) 

Représenter les multiplications sous forme de quadrillages

https://educajou.forge.apps.education.fr/multiples/


### Estimation

![](img/estimation.png) 

Trouver la valeur d'un nombre représenté par une position sur une droite non graduée

https://educajou.forge.apps.education.fr/estimation/

## Maîtrise de la langue

### Syllabux

![](img/syllabux.png) 

Syllabaire interactif à entrée graphémique

https://educajou.forge.apps.education.fr/syllabux/


### Flashmots

![](img/flashmots.png) 

Entraînement de la mémoire orthographique

https://educajou.forge.apps.education.fr/flashmots/


### RapidÉtik

![](img/rapidetik.png) 

Créer en un clic des exercices à étiquettes à partir d'une phrase

https://educajou.forge.apps.education.fr/rapidetik/

### Motminot

![](img/motminot.png) 

Une adaptation de Wordle pour l'école primaire

https://educajou.forge.apps.education.fr/motminot/


## Organisation de la classe et de l'école

### Remix

![](img/remix.png) 

Ventiler des effectifs en groupes hétérogènes

https://educajou.forge.apps.education.fr/remix/

### Répartition

![](img/repartition.png) 

Préparer la répartition des classes de façon semi-automatique

https://educajou.forge.apps.education.fr/repartition/


## Autres outils

### Seyes

![](img/seyes.png) 

Afficher un fond de page Seyes et écrire par-dessus en cursive

https://educajou.forge.apps.education.fr/seyes/

### Lis pour moi

![](img/lispourmoi.png) 

Écouter une phrase saisie et la partager par code QR ou lien

https://educajou.forge.apps.education.fr/lispourmoi/



### AutoBD

![](img/autobd.png) 

Réaliser des bandes dessinées simples en quelques clics

https://educajou.forge.apps.education.fr/autobd/

### Tux Timer

![](img/tux-timer.png) 

Un compte à rebours réglable inspiré des objets de type "Time timer"

https://educajou.forge.apps.education.fr/tuxtimer/

